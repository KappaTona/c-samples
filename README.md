This repository exists because I heard a true saying:
`"Katona rád férne a C ben való programozás"` and now I can say truly `"I have Tried and made Errors`"

Therefore these two sentence can make my reality.

---

The goal is porting and extending [this](https://gist.github.com/Mashpoe/3d949824be514c43b58706eb29c33c43) to Liunux using ncurses.

[reddit thread](https://redd.it/qcpfow)

Also I try to create this without allocating memory on the heap. Therefore I don't use the famos robust [GSL library](https://www.gnu.org/software/gsl/doc/html/vectors.html) yet.

[youtube video playist of the repository progress](https://www.youtube.com/playlist?list=PL7OfFdbG7gk6_rhfS4w7L0x5Vb93PmEww)

Reading about what to expect ![here](./Tesseract/README.md)

----

Personal notes:
some\_time += 0.0004; is good enough speed

good enough for my terminal settings
HEIGHT=30
WIDTH=50

-----
Linux dependencies
- cmake 
- make 
- gcc 
- git 
- glib2 
- pkgconf 
- valgrind 
- check

- for Arch everything can be found in core. Details in ![gitlab-ci.yml](./.gitlab-ci.yml).
