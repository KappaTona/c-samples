cmake_minimum_required(VERSION 3.20)
project(some_c)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

set(CMAKE_C_STANDARD 17)
find_package(PkgConfig REQUIRED)
pkg_search_module(NCURSES REQUIRED ncurses
    IMPORTED_TARGET)
pkg_search_module(CHECK REQUIRED check
    IMPORTED_TARGET)

# why do you need the AddTestWithMemcheck?
# include(CTest) should create a target *memcheck* which runs the unit tests with valgrind
find_program(MEMORYCHECK_COMMAND valgrind REQUIRED)
set(MEMORYCHECK_COMMAND_OPTIONS --leak-check=full --trace-children=yes)

add_subdirectory(Tesseract)
