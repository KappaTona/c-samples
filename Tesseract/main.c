#include "tesseract_module.h"
#include "geometry_types.h"
#include "rendering_helper_module.h"
#include <curses.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>


void run_ncurse();
int main()
{
    run_ncurse();
}

void fill(char projection_space[HEIGHT][WIDTH], char f)
{
    for (int i = 0; i < HEIGHT; ++i)
        for (int j = 0; j < WIDTH; ++j)
            projection_space[i][j] = f;
}

void run_ncurse()
{
    RenderingHelper rendering = init_rendering_helper((WindowBounds){HEIGHT+5, WIDTH+5});
    TesseractProjection tesseract = init_tesseract_projection();
    TesseractData tesseract_data = init_tesseract_data();
    TesseractProjection reciproc_tesseract = init_tesseract_projection();
    TesseractData reciproc_tesseract_data = init_tesseract_data();

    char projection_space[HEIGHT][WIDTH] = {{0}};
    double some_time = 0.;
    bool one_more = false;
    bool running = rendering.for_rendering(projection_space);

    while (running)
    {
        some_time += 0.0004;
        double const delay = .3;

        tesseract.prepare_for_rendering(&tesseract_data, some_time, delay);
        reciproc_tesseract.prepare_for_rendering(&reciproc_tesseract_data,
            -some_time, -delay);

        fill(projection_space, ' ');

        render_projection_into_char_array(projection_space, &tesseract_data);
        if (one_more)
            render_projection_into_char_array(projection_space, &reciproc_tesseract_data);

        running = rendering.for_rendering(projection_space);
        one_more = rendering.requested_one_more_tesseract();
    }

    cleanup_rendering_helper(&rendering);
    cleanup_tesseract_projection(&tesseract);
    cleanup_tesseract_projection(&reciproc_tesseract);
}
