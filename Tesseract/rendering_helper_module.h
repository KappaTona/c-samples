#ifndef rendering_helper_module_h
#define rendering_helper_module_h
#include "geometry_types.h"
#include <stdbool.h>

#define init_rendering_helper init_rendering_helper_functions
#define cleanup_rendering_helper cleanup_rendering_helper_functions

typedef struct WindowError
{
    char const *msg;
    int y, x;
    char position_character;
} WindowError;

typedef struct WindowBounds 
{
    int y, x;
} WindowBounds;

typedef struct RenderingHelper
{
    bool (*for_rendering)(char [HEIGHT][WIDTH]);
    bool (*requested_one_more_tesseract)();
} RenderingHelper;

extern RenderingHelper init_rendering_helper(WindowBounds);
extern void cleanup_rendering_helper(RenderingHelper *function_set);


#endif //rendering_helper_module_h
