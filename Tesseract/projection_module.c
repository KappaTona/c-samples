#include "projection_module.h"
#include "geometry_types.h"
#include "vector_operations.h"
#include <math.h>
#include <stdlib.h>

static VectorOfDouble4D const origin_for_scaling = { 5, 0, 0, 0 };
static VectorOfDouble3D const origin_for_zooming = { 3.00, 0.99, 1.82 };

static VectorOfDouble4D rotation_for_a_side(
        MatrixOfDouble4D const *rotation, int side_index);

static void inner_prepare_for_rendering(TesseractData *data,
    double const some_time, double const some_time_scale);

static MatrixOfDouble3D make_3D_view_matrix();
static MatrixOfDouble4D make_4D_view_matrix();

TesseractData init_tesseract_data()
{
    MatrixOfDouble3D inner_view_matrix_3D = make_3D_view_matrix();
    MatrixOfDouble4D inner_view_matrix_4D = make_4D_view_matrix();

    return (TesseractData){
        .vertecies_2D={{0}},
        .vertecies_3D={{0}},
        .view_matrix_3D = inner_view_matrix_3D,
        .view_matrix_4D = inner_view_matrix_4D,
    };
}

TesseractProjection init_tesseract_projection()
{
    return (TesseractProjection){
        .prepare_for_rendering=&inner_prepare_for_rendering,
    };
}

void cleanup_tesseract_projection(TesseractProjection *data)
{
    if (data)
    {
        data->prepare_for_rendering = NULL;
        data = NULL;
    }
}

static void inner_three_dimension(TesseractData *data,
        MatrixOfDouble4D const *rotation)
{
    double recip_tan = 1 / tan((PI/3) / 2);

    VectorOperations4D ops = init_vector_operations_4D();
    for (int i = 0; i < 16; ++i)
    {
        VectorOfDouble4D vertex_position = ops.extract(
                (PairOfVector4D) {rotation_for_a_side(rotation, i), origin_for_scaling}
        );

        double scalar = recip_tan / 
            ops.dot((PairOfVector4D){vertex_position, data->view_matrix_4D.col_3});

        data->vertecies_3D[i][0] = scalar *
            ops.dot((PairOfVector4D){vertex_position, data->view_matrix_4D.col_0});
        data->vertecies_3D[i][1] = scalar *
            ops.dot((PairOfVector4D){vertex_position, data->view_matrix_4D.col_1});
        data->vertecies_3D[i][2] = scalar *
            ops.dot((PairOfVector4D){vertex_position, data->view_matrix_4D.col_2});
    }
    cleanup_vector_operations_4D(&ops);
}

static void inner_two_dimension(TesseractData *data, MatrixOfDouble3D const *rotation)
{
    double recip_tan = 1 / tan((PI / 4.) / 2);
    double rotation_array[3][3] = {
        {rotation->col_0._0, rotation->col_0._1, rotation->col_0._2},
        {rotation->col_1._0, rotation->col_1._1, rotation->col_1._2},
        {rotation->col_2._0, rotation->col_2._1, rotation->col_2._2},
    };
    VectorOperations3D ops = init_vector_operations_3D();

    double vertex_position[3] = {0};
    for (int i = 0; i < 16; ++i)
    {
        for (int row = 0; row < 3; ++row)
        {
            vertex_position[row] = 0;
            for (int col = 0; col < 3; ++col)
            {
                vertex_position[row] += rotation_array[row][col] * data->vertecies_3D[i][col];
            }
        }

        VectorOfDouble3D vertex_position_origin = ops.extract((PairOfVector3D)
                {{vertex_position[0], vertex_position[1], vertex_position[2]},
                origin_for_zooming});
		double scalar = recip_tan / ops.dot((PairOfVector3D)
                {vertex_position_origin, data->view_matrix_3D.col_2});

        data->vertecies_2D[i][0] = (WIDTH / 2.) + (WIDTH * scalar *
                ops.dot((PairOfVector3D)
                    {vertex_position_origin, data->view_matrix_3D.col_0}));

        data->vertecies_2D[i][1] = (HEIGHT / 2.) + (HEIGHT * scalar *
                ops.dot((PairOfVector3D)
                    {vertex_position_origin, data->view_matrix_3D.col_1}));
    }
    cleanup_vector_operations_3D(&ops);
}

static VectorOfDouble4D rotation_for_a_side(MatrixOfDouble4D const *rotation, int i)
{
    static VectorOfDouble4D const hyper_cube_verticies[16] = {
        {-1, -1, -1, -1},
        { 1, -1, -1, -1},
        {-1,  1, -1, -1},
        { 1,  1, -1, -1},
        {-1, -1,  1, -1},
        { 1, -1,  1, -1},
        {-1,  1,  1, -1},
        { 1,  1,  1, -1},
        {-1, -1, -1,  1},
        { 1, -1, -1,  1},
        {-1,  1, -1,  1},
        { 1,  1, -1,  1},
        {-1, -1,  1,  1},
        { 1, -1,  1,  1},
        {-1,  1,  1,  1},
        { 1,  1,  1,  1},
    };

    double current_cube_side [4] ={
        hyper_cube_verticies[i]._0,
        hyper_cube_verticies[i]._1,
        hyper_cube_verticies[i]._2,
        hyper_cube_verticies[i]._3
    };

    double rotation_array[4][4] ={
        {rotation->col_0._0, rotation->col_0._1, rotation->col_0._2, rotation->col_0._3},
        {rotation->col_1._0, rotation->col_1._1, rotation->col_1._2, rotation->col_1._3},
        {rotation->col_2._0, rotation->col_2._1, rotation->col_2._2, rotation->col_2._3},
        {rotation->col_3._0, rotation->col_3._1, rotation->col_3._2, rotation->col_3._3},
    };

    double rotation_vector[4] ={0};
    for (int row = 0; row < 4; ++row)
        for (int col = 0; col < 4; ++col)
            rotation_vector[row] += rotation_array[row][col] * current_cube_side[col];

    return (VectorOfDouble4D){
        rotation_vector[0], rotation_vector[1], rotation_vector[2], rotation_vector[3]};
}

static MatrixOfDouble4D make_4D_view_matrix()
{
    static VectorOfDouble4D const down = { 0, 0, 1, 0 };

    MatrixOfDouble4D view_matrix = {0};
    VectorOperations4D ops = init_vector_operations_4D();

    // get the normalized 4th column-vector.
    view_matrix.col_3 = ops.extract((PairOfVector4D)
            {{ 0, 0, 0, 0 }, origin_for_scaling});
    ops.scale(&view_matrix.col_3, 1 / ops.normalize(view_matrix.col_3));

    // calculate the normalized 1st column-vector.
    view_matrix.col_0 = ops.cross((TrioOfVector4D){
            { 0, 1, 0, 0 }, down, view_matrix.col_3});
    ops.scale(&view_matrix.col_0, 1 / ops.normalize(view_matrix.col_0));;

    // calculate the normalized 2nd column-vector.
    view_matrix.col_1 = ops.cross((TrioOfVector4D)
            {down, view_matrix.col_3, view_matrix.col_0});
    ops.scale(&view_matrix.col_1, 1 / ops.normalize(view_matrix.col_1));

    // calculate the 3rd column-vector.
    view_matrix.col_2 = ops.cross((TrioOfVector4D)
            {view_matrix.col_3, view_matrix.col_0, view_matrix.col_1});

    cleanup_vector_operations_4D(&ops);
    return view_matrix;
}

static MatrixOfDouble3D make_3D_view_matrix()
{
    VectorOperations3D ops = init_vector_operations_3D();
    MatrixOfDouble3D view_matrix = {0};
    // Get the normalized 3rd column-vector.
    view_matrix.col_2 = ops.extract((PairOfVector3D){
            { 0, 0, 0 }, origin_for_zooming});
    ops.scale(&view_matrix.col_2, 1 / ops.normalize(view_matrix.col_2));

    // Calculate the normalized 1st column-vector.
    view_matrix.col_0 = ops.cross((PairOfVector3D){view_matrix.col_2, { 0, -1, 0 }});
    ops.scale(&view_matrix.col_0, 1 / ops.normalize(view_matrix.col_0));

    // Calculate the 2nd column-vector.
    view_matrix.col_1 = ops.cross((PairOfVector3D){view_matrix.col_0, view_matrix.col_2});

    cleanup_vector_operations_3D(&ops);
    return view_matrix;
}

static void inner_prepare_for_rendering(TesseractData *data,
    double const some_time, double const some_time_scale)
{
    MatrixOfDouble4D rotated_matrix4D_XW = {
        .col_0= {._0 = cos(some_time), ._3 = -sin(some_time)},
        .col_1= {._1=1.},
        .col_2 = {._2=1.},
        .col_3= {._0 = sin(some_time), ._3 = cos(some_time)},
    };
    inner_three_dimension(data, &rotated_matrix4D_XW);

    double some_time_otherwise = some_time * some_time_scale;
    MatrixOfDouble3D rotated_matrix3D_XZ = {
        {cos(some_time_otherwise), 0, -sin(some_time_otherwise)},
        {0, 1, 0},
        {sin(some_time_otherwise), 0, cos(some_time_otherwise)}
    };
    inner_two_dimension(data, &rotated_matrix3D_XZ);
}
