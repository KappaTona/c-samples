#ifndef vector_operations_h
#define vector_operations_h
#include "geometry_types.h"


#define init_vector_operations_3D init_vector_operations_3D_functions
#define init_vector_operations_4D init_vector_operations_4D_functions
#define cleanup_vector_operations_3D cleanup_vector_operations_3D_functions
#define cleanup_vector_operations_4D cleanup_vector_operations_4D_functions

typedef struct PairOfVector3D
{
    VectorOfDouble3D first, second;
} PairOfVector3D;

typedef struct VectorOperations3D
{
    VectorOfDouble3D (*extract)(PairOfVector3D const);
    double (*dot)(PairOfVector3D const);
    double (*normalize)(VectorOfDouble3D const);
    VectorOfDouble3D (*cross)(PairOfVector3D const);
    void (*scale)(VectorOfDouble3D *, double const);
} VectorOperations3D;

extern VectorOperations3D init_vector_operations_3D();
extern void cleanup_vector_operations_3D(VectorOperations3D*);

typedef struct PairOfVector4D
{
    VectorOfDouble4D first, second;
} PairOfVector4D;

typedef struct TrioOfVector4D
{
    VectorOfDouble4D first, second, third;
} TrioOfVector4D;

typedef struct VectorOperations4D
{
    VectorOfDouble4D (*extract)(PairOfVector4D const);
    double (*dot)(PairOfVector4D const);
    double (*normalize)(VectorOfDouble4D const);
    VectorOfDouble4D (*cross)(TrioOfVector4D const);
    void (*scale)(VectorOfDouble4D *, double const);
} VectorOperations4D;

extern VectorOperations4D init_vector_operations_4D();
extern void cleanup_vector_operations_4D(VectorOperations4D*);


#endif //vector_operations_h
