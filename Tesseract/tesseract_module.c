#include "tesseract_module.h"
#include <stdlib.h>

typedef struct rendering_bounds_s
{
    int y, x;
} rendering_bounds_s;


static void ln(char projection_space[HEIGHT][WIDTH], rendering_bounds_s a, rendering_bounds_s b);

void render_projection_into_char_array(char projection_space[HEIGHT][WIDTH],
        TesseractData const *data)
{
    static int const indices[32][2] =
    {
        // cube #1
        {0, 1},
        {0, 2},
        {0, 4},
        {1, 3},
        {1, 5},
        {2, 3},
        {2, 6},
        {3, 7},
        {4, 5},
        {4, 6},
        {5, 7},
        {6, 7},

        // in-between lines
        {0,	8},
        {1,	9},
        {2,	10},
        {3,	11},
        {4,	12},
        {5,	13},
        {6,	14},
        {7,	15},

        // cube #2
        {8, 9},
        {8, 10},
        {8, 12},
        {9, 11},
        {9, 13},
        {10, 11},
        {10, 14},
        {11, 15},
        {12, 13},
        {12, 14},
        {13, 15},
        {14, 15},
    };

    for (int i = 0; i < 32; ++i)
    {
        int a = indices[i][0];
        int b = indices[i][1];
        rendering_bounds_s c1 = { data->vertecies_2D[a][0], data->vertecies_2D[a][1] };
        rendering_bounds_s c2 = { data->vertecies_2D[b][0], data->vertecies_2D[b][1] };
        ln(projection_space, c1, c2);
    }
}

static char getpoint(rendering_bounds_s const bound[3],double err)
{
    if (abs(bound[0].y - bound[2].y) < 2)
        return err > .5 ? ';' : '|';

    if (abs(bound[0].x - bound[2].x) < 2 &&
        (bound[0].x >= bound[2].x || bound[1].x != bound[2].x) &&
        (bound[0].x <= bound[2].x || bound[1].x != bound[0].x))
    {
        return '_';
    }

    int x = bound[0].y < bound[2].y ? bound[0].x : bound[2].x;
    return x < bound[1].x ? '\\' : '/';
}

static void ln(char points[HEIGHT][WIDTH], rendering_bounds_s a, rendering_bounds_s b)
{
    points[a.x][a.y] = '@';
    points[b.x][b.y] = '@';

    int diff_x = abs(b.x - a.x), affix_x = a.x < b.x ? 1 : -1;
    int diff_y = abs(b.y - a.y), affix_y = a.y < b.y ? 1 : -1;
    int err = (diff_x > diff_y ? diff_x : -diff_y) / 2;

    rendering_bounds_s bound[3] = {0};
    double ers[3] = {0};

    for (int i = 0; i < 3; ++i)
    {
        bound[i] = a;
        ers[i] = (double)(err - diff_x) / (diff_y - diff_x);
        ers[i] = affix_y == 1 ? 1. - ers[i] : ers[i];

        if (a.y == b.y && a.x == b.x)
            return;

        double prev_err = err;
        if (prev_err > -diff_x) { err -= diff_y; a.x += affix_x; }
        if (prev_err < diff_y) { err += diff_x; a.y += affix_y; }
    }

    for (;;)
    {
        
        points[bound[1].x][bound[1].y] = getpoint(bound, ers[1]);

        bound[0] = bound[1];
        bound[1] = bound[2];
        bound[2] = a;

        ers[0] = ers[1];
        ers[1] = ers[2];
        ers[2] = (double)(err - diff_x) / (diff_y - diff_x);
        ers[2] = affix_y == 1 ? 1. - ers[2] : ers[2];

        if (a.x == b.x && a.y == b.y)
            break;

        double prev_err = err;
        if (prev_err > -diff_x) { err -= diff_y; a.x += affix_x; }
        if (prev_err < diff_y) { err += diff_x; a.y += affix_y; }
    }

    // add the final point
    points[bound[1].x][bound[1].y] = getpoint(bound, ers[1]);
}
