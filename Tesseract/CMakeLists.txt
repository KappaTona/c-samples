enable_testing()
add_library(tesseract tesseract_module.c projection_module.c rendering_helper_module.c)
add_library(vector_operations vector_operations.c)
#TODO
#   linking to math library this shold be replaced with GSL but check
#   the test stuff won't link to exec. I tried include .c should include .h
target_link_libraries(vector_operations m)
target_link_libraries(tesseract vector_operations)
target_compile_options(tesseract PUBLIC -Wall -Wextra -Wno-empty-translation-unit -pedantic )
target_compile_options(vector_operations PUBLIC -Wall -Wextra -Wno-empty-translation-unit -pedantic )


add_executable(tesseract_ncurses main.c)
target_link_libraries(tesseract_ncurses PUBLIC PkgConfig::NCURSES tesseract)

add_executable(check_tesseract check_tesseract.c)
add_test(NAME check_tesseract COMMAND check_tesseract)
target_link_libraries(check_tesseract PkgConfig::CHECK tesseract)

add_executable(check_vector_operations check_vector_operations.c)
add_test(NAME check_vector_operations COMMAND check_vector_operations)
target_link_libraries(check_vector_operations PkgConfig::CHECK vector_operations)
