What to expect?
---

After starting the program, one tesseract will rotate clockwise.
You can choose to add one more Tesseract which will rotate counter clockwise.

The tesseracts act identically, two reasons:
- I don't want to use [VLA](https://en.wikipedia.org/wiki/Variable-length_array)
- And the original idea does not extend otherwise.

Limitations and posible ways to improve
---
- The program won't handle resize events because I don't understand `math nor ncurse` deeply enough.
- maximum two tesseracts, and they differ in time only
- Observe if you will, that valgrind shows some leaks, more accurately "still reachable" memory sections. This is known and further details can be read [here](https://invisible-island.net/ncurses/ncurses.faq.html#config_leaks).

Configuration possibility
---
- In ![geometry\_types.h:5,6](./geometry_types.h#L5) you can choose HEIGHT and WIDTH for the rendering bounds
- you can ![hard interrupt](./rendering_helper_module.c#L108) the rotations by pressing `up arrow` this will cause the program to gracefully exit.
- you can add the second tesseract into rendering by pressing `right arrow`
- you can remove the second tesseract from rendering by pressing `left arrow`
- you can ![soft interrupt](./rendering_helper_module.c#L113) the rotations by pressing almost any key EXCEPT the mentioned ones above.
- these key combinations can be foud inside the ![listening](./rendering_helper_module.c#151) function.
