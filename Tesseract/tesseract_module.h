#ifndef tesseract_module_h
#define tesseract_module_h
#include "geometry_types.h"
#include "projection_module.h"

// TODO curses have stuff like resize events which would be a very nice feature to add
// harder with the heap though


extern void render_projection_into_char_array(
        char d[HEIGHT][WIDTH], TesseractData const*);


#endif //tesseract_module_h
