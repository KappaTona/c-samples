#include "tesseract_module.h"
#include <check.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>


double tolarance = 0.0003;

START_TEST(test_view3D)
{
    MatrixOfDouble3D result = init_tesseract_data().view_matrix_3D;

    ck_assert_double_eq_tol(result.col_0._0, -0.518683, tolarance);
    ck_assert_double_eq_tol(result.col_0._1, 0., tolarance);
    ck_assert_double_eq_tol(result.col_0._2, 0.854968, tolarance);
    ck_assert_double_eq_tol(result.col_1._0, 0.232157, tolarance);
    ck_assert_double_eq_tol(result.col_1._1, -0.962427, tolarance);
    ck_assert_double_eq_tol(result.col_1._2, 0.140842, tolarance);
    ck_assert_double_eq_tol(result.col_2._0, -0.822845, tolarance);
    ck_assert_double_eq_tol(result.col_2._1, -0.271539, tolarance);
    ck_assert_double_eq_tol(result.col_2._2, -0.499193, tolarance);
}
END_TEST

START_TEST(test_view4D)
{
    MatrixOfDouble4D result = init_tesseract_data().view_matrix_4D;

    ck_assert_double_eq(result.col_0._0, 0.);
    ck_assert_double_eq(result.col_0._1, 0.);
    ck_assert_double_eq(result.col_0._2, 0.);
    ck_assert_double_eq(result.col_0._3, 1.);
    ck_assert_double_eq(result.col_1._0, 0.);
    ck_assert_double_eq(result.col_1._1, -1.);
    ck_assert_double_eq(result.col_1._2, 0.);
    ck_assert_double_eq(result.col_1._3, 0.);
    ck_assert_double_eq(result.col_2._0, 0.);
    ck_assert_double_eq(result.col_2._1, 0.);
    ck_assert_double_eq(result.col_2._2, -1.);
    ck_assert_double_eq(result.col_2._3, 0.);
    ck_assert_double_eq(result.col_3._0, -1.);
    ck_assert_double_eq(result.col_3._1, 0.);
    ck_assert_double_eq(result.col_3._2, 0.);
    ck_assert_double_eq(result.col_3._3, 0.);
}
END_TEST



Suite *tesseract_suite()
{
    Suite *s = suite_create("Tesseract");
    TCase *tc_core = tcase_create("Core");

    tcase_add_test(tc_core, test_view3D);
    tcase_add_test(tc_core, test_view4D);
    

    suite_add_tcase(s, tc_core);
    return s;
}


int main()
{
    Suite *s = tesseract_suite();
    SRunner *sr = srunner_create(s);

    srunner_run_all(sr, CK_VERBOSE);
    int number_failed = srunner_ntests_failed(sr);

    srunner_free(sr);
    return (number_failed ? EXIT_FAILURE : EXIT_SUCCESS);
}
